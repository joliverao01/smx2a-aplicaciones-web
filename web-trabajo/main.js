////////////////////////////////////////////////// ON SCROLL ////////////////////////////////////////////////////////////////////
window.onscroll = function() {myFunction()};

function myFunction() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  var scrolled = (winScroll / height) * 100;
  document.getElementById("barra-progreso").style.width = scrolled + "%";
  // $("#barra-progreso").css({width: scrolled + '%'});
  if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
    $('#cabecera').css('animation', 'fade-out 0.5s steps(1000) forwards');
    $("#barra-progreso").css({'display': 'block', 'animation': 'cabecera-lights 7s ease-in-out infinite alternate'});
    $('#barra-progreso, #contenedor-menu').css({'position': 'sticky', 'top': '0', 'display': 'block', 'z-index': '1'});
  }

  else {
    $('#cabecera').css({'display': 'block', 'animation': 'fade-in 0.5s steps(1000) forwards'});
    $("#barra-progreso").css({'display': 'none', 'animation': 'cabecera-lights paused, fade-out 1s steps(1000) forwards'});
    $('#abrir, #barra-progreso, #contenedor-menu').css({'position': 'static', 'top': '0'});
  }

  };

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$( document ).ready(function() {
  ////////////////////////////////////////////////////////////LAS ENTRADAS DEL MENU DE NAVEGACIÓN////////////////////////////////////////////////////////////////////////////////////
  $('#context-and-justification').click(function() {
   $('html, body').animate({scrollTop: ($('#titulo-context-i-justificacio-del-treball').offset().top)},200);
  });

  $('#target').click(function() {
    $('html, body').animate({scrollTop: ($('#titulo-objectius-del-treball').offset().top)},200);
  });

  $('#planning').click(function() {
    $('html, body').animate({scrollTop: ($('#titulo-planificacio-del-projecte').offset().top)},200);
  });

  $('#material').click(function() {
    $('html, body').animate({scrollTop: ($('#titulo-materials').offset().top)},2);
  });

  $('#economic-valuation').click(function() {
    $('html, body').animate({scrollTop: ($('#titulo-valoracio-economica-del-proyecte').offset().top)},200);
  });

  $('#development').click(function() {
    $('html, body').animate({scrollTop: ($('#titulo-desenvolupament-del-proyecte').offset().top)},200);
  });
  ////////////////////////////////////////////////////////////////////////////PARA MOSTRAR Y OCULTAR EL MENÚ///////////////////////////////////
  var pressed = 0;
  $('#boton-menu, .entrada-menu').click(function(){
    pressed++;
    if (pressed === 1) {
      $('#contenedor-texto-1, #footer, #cabecera, #contenedor-menu').css({'filter': 'blur(2px)'})
      $('#menu-navegacion').css({'animation': 'show 0.4s steps(1000) 1 forwards'});
    };

    if (pressed === 2) {
      $('#contenedor-texto-1, #footer, #cabecera, #contenedor-menu').css({'filter': 'blur(0px)'})
      $('#menu-navegacion').css({'animation': 'hide 0.4s steps(1000) 1 forwards'});
      pressed = pressed - 2;
    };


   });
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
});
