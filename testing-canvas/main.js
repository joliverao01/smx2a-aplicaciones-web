var canvas = document.getElementById("canvas1");
const ctx = canvas.getContext('2d');
var numAle200 = Math.floor((Math.random() * 200) + Math.random());
var numAle100 = Math.floor((Math.random() * 100) + Math.random());
var numAle10 = Math.floor((Math.random() * 10) + Math.random());
for (let a = 0; a < 30; a++) {
  for (let b = 0; b < 50; b++) {
      ctx.fillStyle = `rgb(${a * numAle100},${b * numAle200},${a * numAle10},${b * Math.random()})`;
      ctx.fillRect(b * numAle200, a * numAle200, numAle10, numAle10);

      ctx.font = numAle200 + "px Fantasy";
      ctx.strokeText("muelte",numAle200,numAle200);

      ctx.beginPath();
      ctx.arc(numAle100, numAle100, numAle100, numAle200, Math.random() * Math.PI);
      ctx.stroke();
    }
  }

function actualizar(){location.reload(true);}
setInterval("actualizar()",50);
