$(document).ready(function(){
  $("#seleccionarvideo").change(function(){  // Cuando seleccionarvideo cambie se ejecuta la siguiete funcion:
    // Cuando el value de seleccionarvideo == vidnone
    if ($(this).val() == "vidnone") {
      $("#viddarksouls").css("display", "none");
      $("#viddarksoulsii").css("display", "none");
      $("#viddarksoulsiii").css("display", "none");
      $("#viddemonsouls").css("display", "none");
      $("#vidbloodborne").css("display", "none");
    }

    // Cuando el value de seleccionarvideo == darksouls
    if ($(this).val() == "darksouls") {
      $("#viddarksouls").css("display", "block");
      $("#viddarksoulsii").css("display", "none");
      $("#viddarksoulsiii").css("display", "none");
      $("#viddemonsouls").css("display", "none");
      $("#vidbloodborne").css("display", "none");
    }

    // Cuando el value de seleccionarvideo == darksoulsii
    if ($(this).val() == "darksoulsii") {
      $("#viddarksouls").css("display", "none");
      $("#viddarksoulsii").css("display", "block");
      $("#viddarksoulsiii").css("display", "none");
      $("#viddemonsouls").css("display", "none");
      $("#vidbloodborne").css("display", "none");
    }

    // Cuando el value de seleccionarvideo == darksoulsiii
    if ($(this).val() == "darksoulsiii") {
      $("#viddarksouls").css("display", "none");
      $("#viddarksoulsii").css("display", "none");
      $("#viddarksoulsiii").css("display", "block");
      $("#viddemonsouls").css("display", "none");
      $("#vidbloodborne").css("display", "none");
    }

    // Cuando el value de seleccionarvideo == demonsouls
    if ($(this).val() == "demonsouls") {
      $("#viddarksouls").css("display", "none");
      $("#viddarksoulsii").css("display", "none");
      $("#viddarksoulsiii").css("display", "none");
      $("#viddemonsouls").css("display", "block");
      $("#vidbloodborne").css("display", "none");
    }

    // Cuando el value de seleccionarvideo == bloodborne
    if ($(this).val() == "bloodborne") {
      $("#viddarksouls").css("display", "none");
      $("#viddarksoulsii").css("display", "none");
      $("#viddarksoulsiii").css("display", "none");
      $("#viddemonsouls").css("display", "none");
      $("#vidbloodborne").css("display", "block");
    }
  });
});
