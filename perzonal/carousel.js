// Seleccionamos todos los elementos de tipo carousel-image.
var cimages = document.getElementsByClassName('carousel-image');
// Añadimos los listener a los botones para que estén linkados a las funciones next i previous
document.getElementById('button-next').addEventListener("click", next);
document.getElementById('button-previous').addEventListener("click", previous);

// Inicializamos el índice con el valor 0 (la primera imagen)
var index = 0;

function next() {
  // Oculta la imagen actual
  cimages[index].style.display = 'none';
  // Suma uno al index para mostrar la siguiente foto
   index = index + 1;

   // Si el índice es igual a la longitud del vector he llegado al final de la lista de imágenes
   if (index == cimages.length) {
     // Actualizamos el índice a 0 para apunta a la primera imagen
     index = 0;
   }

   // Mostramos la siguiente imagen
   cimages[index].style.display = 'block';
}

function previous() {
  // Oculta la imagen actual
  cimages[index].style.display = 'none';
  // Resta uno al index para mostrar la foto anterior
  index = index - 1;

  // Si el índice es menor que 0 hemos llegado a la primera imagen, la siguiente en mostrar es la última
  if (index < 0) {
    // Actualizamos el índice con el valor de la última imagen
    index = cimages.length - 1;
  }

  // Mostramos la imagen anterior
  cimages[index].style.display = 'block';
}

// Pasamos la imagen cada 5 segundos
  setInterval(function() {
  next();
  }, 5000);

  
