$(document).ready(function(){
  $("#display").val(""); //borra el display
  $(".numeros, .funciones").click(function(){
    var numeroantiguo = $("#display").val(); //crea una variable y hace que salgan los numeros y las funciones
    $("#display").val(numeroantiguo+$(this).val()); //hace que salga la función por el display
    });

    $("#reiniciar").click(function(){
      $("#display").val(""); //borra el display
      });

    $("#enter").click(function(){
      var numeroantiguo = $("#display").val(); //crea una variable y hace que salgan los numeros y las funciones
      var resultado = eval(numeroantiguo); //hace que se ejecute la función
      $("#display").val(resultado); //hace que salga el resultado por el display
    });

    $("#borrar").click(function(){
      var numeroantiguo = $("#display").val();
      var borraruno = numeroantiguo.substring(0, numeroantiguo.length-1);
      $("#display").val(borraruno);
    });

});
