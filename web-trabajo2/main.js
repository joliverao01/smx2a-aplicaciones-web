////////////////////////////////////////////////// ON SCROLL ////////////////////////////////////////////////////////////////////
window.onscroll = function() {myFunction()};

function myFunction() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  var scrolled = (winScroll / height) * 100;
  document.getElementById("barra-progreso").style.width = scrolled + "%";
  // if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
  //
  // }
  //
  // else {
  //
  // }

  };

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  $(window).scroll(function(event) {
    var valueScrollTop = $(window).scrollTop();

    if (valueScrollTop > 180) {
      $('#contenedor-cabecera').css({'position': 'sticky', 'top':'0'});
      $('#cabecera').css({'height':'70px'})
      $('.entrada-menu').css({'margin-bottom': 'none', 'margin-top': 'none', 'margin-left': '6%', 'text-align': 'left'});
      $('#context-and-justification').css({'margin-left': '-7%'});
      $('h1, h2').css({'text-align': 'left', 'float': 'left', 'margin-left': '2%'})
      $('h1').css({'font-size': '300%', 'margin-top': '1.2%'})
      $('h2').css({'font-size': '200%', 'margin-top': '1.8%'})
      $('h3').css({'font-size': '100%', 'margin-top': '2%', 'text-align': 'right', 'float': 'right'})
    };

    if (valueScrollTop < 150) {
      $('#contenedor-cabecera').css({'position': 'relative'});
      $('#cabecera').css({'height':'143px'});
      $('.entrada-menu').css({'margin-bottom': '0.5%', 'margin-top': '0.5%', 'margin-left': '5%', 'text-align': 'center'});
      $('h1, h2').css({'float': 'none', 'margin-left': 'none'})
      $('h1').css({'text-align': 'center', 'font-size': '400%', 'margin-top': '3%'})
      $('h2').css({'text-align': 'center', 'font-size': '200%', 'margin-top': 'none'})
      $('h3').css({'margin-top': '-2%', 'margin-right': '2%', 'text-align': 'right', 'float': 'none'})
    };

  });


$( document ).ready(function() {
  var ventana_alto = $('body').height();
  // if ventana_alto ==>


  ////////////////////////////////////////////////////////////LAS ENTRADAS DEL MENU DE NAVEGACIÓN////////////////////////////////////////////////////////////////////////////////////
  $('#context-and-justification').click(function() {
   $('html, body').animate({scrollTop: ($('#div-de-toda-la-pagina').offset().top)},200);
  });

  $('#target').click(function() {
    $('html, body').animate({scrollTop: ($('#titulo-objectius-del-treball').offset().top)},200);
  });

  $('#planning').click(function() {
    $('html, body').animate({scrollTop: ($('#titulo-planificacio-del-projecte').offset().top)},200);
  });

  $('#material').click(function() {
    $('html, body').animate({scrollTop: ($('#titulo-materials').offset().top)},200);
  });

  $('#economic-valuation').click(function() {
    $('html, body').animate({scrollTop: ($('#titulo-valoracio-economica-del-proyecte').offset().top)},200);
  });

  $('#development').click(function() {
    $('html, body').animate({scrollTop: ($('#titulo-desenvolupament-del-proyecte').offset().top)},200);
  });
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
});
