$(document).ready(function(){
  var numAle = Math.floor((Math.random() * 16) + 1);

  var approaching = {url:"url('img/approaching.png') center no-repeat", size:"80%", num: 1};
  var boquiabierto = {url:"url('img/boquiabierto.png')", num: 2};
  var comerciante = {url:"url('img/comerciante.png')", num: 3};
  var dieforasong = {url:"url('img/dieforasong.png')", num: 4};
  var skel = {url:"url('img/skel.png')", num: 5};
  var tauro = {url:"url('img/tauro.png')", num: 6};
  var bull = {url:"url('img/bull.png')", num: 7};
  var croac = {url:"url('img/croac.png')", num: 8};

  var lista = [approaching.url,boquiabierto.num,comerciante.num,dieforasong.num,skel.num,tauro.num,,bull.num,croac.num];
  lista = lista.sort(function() {return Math.random() - 0.2});
  console.log(lista);

  $("#start").click(function() {
    $(".card").css({"animation": "orientation-on 0.7s steps(1000) 1 forwards", "background": "linear-gradient(rgba(36, 36, 36, 0.5), rgba(100, 30, 30, 0.5))"});
    $("#start").css({"display": "none"});
    $("#timer").css({"display": "block"});

    var timer = setInterval(function() {
        var timerStarts = 60;
        var timerEnds = 1;
        var distance = timerStarts - timerEnds;
        $("#timer").html(distance);

    }, 1000);


      function poderGirarCartas(){
        $(".card").css({"transform": "scaleX(1)"});
        $("#card1").click(function(){
          $("#card1").css({"background": approaching.url,"background-size": approaching.size});
        });

        $("#card2").click(function(){
          $("#card2").css({"background": tauro.url,"background-size": tauro.size});
        });
        $("#card3").click(function(){
          $("#card3").css({"background": skel.url,"background-size": skel.size});
        });
      } setTimeout(poderGirarCartas, 1000);

  });



});
