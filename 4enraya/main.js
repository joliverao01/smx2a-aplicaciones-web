console.log("Welcome to jQuery.");

$(document).ready(function(){

  $("#new-article").click(function(){

    // alert( $("#form-title").val() );
    // alert( $("#form-description").val() );

    // Crea una variable que se llama titulo y afecta al titulo
    var titulo = $("#form-title").val();
    // Crea una variable que se llama description y afecta a la descripción
    var description = $("#form-description").val();
    // Crea una variable que se llama url y afecta a la imagen
    var url = $("#form-url").val();

    var plantilla = "";
    plantilla += "<article>";
    // Pone como h2 todo lo que escribas dentro de titulo
    plantilla += "<h2>";
    plantilla += titulo;
    plantilla += "</h2>";

    // Pone como p todo lo que escribas dentro de descripcion
    plantilla += "<p>";
    plantilla += description;
    plantilla += "</p>";

    // Pone como img el link que pegues dentro
    plantilla += "<img src='"+url+"'</img>";
    plantilla += "</article>";

    // Hace que le pase a plantilla
    $("#content").append(plantilla);

    // Resetear los valores del formulario
    $("#form-title").val("");
    $("#form-description").val("");
    $("#form-url").val("");

    // Para evitar que la página se recargue;
    return false;
  });
});
