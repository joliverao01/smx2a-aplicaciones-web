$(document).ready(function(){
  var blurRadius = 10;
  var comboFontSize = 100;
  var restos = 0; //Los puntos base
  var biometal = 0; //Es el equivalente a 10 restos
  var chips = 0; //Es el equivalente a 1000 biometales o un combo de 200
  var subTanks = 0; //Es el equivalente a 1200 chips
  var biometalX = 0; //Es el equivalente a 950 subtanks
  var combo = 0; //Los puntos de combo al clicar
  ///////////////////////////////////////// OBJETOS //////////////////////////////////////////////////////
  ////////////////////////////// Precio //////////////////////////
  var precioPistola = 5; //biometal
  var precioCancionMaluma = 30; //biometal
  var precioEspada = 100; //biometal
  var precioSpam = 900; //biometal
  var precioFlame = 5; //chips
  var precioClaymore = 30; //chips
  var precioMaincra = 100; //chips
  var precioCubito = 900; //chips
  var precioMuelte = 5; //subtanks
  var precioSamael = 30; //subtanks
  var precioWalker = 100; //subtanks
  var precioW4LK3R = 900; //subtanks
  var precioFlambergue = 5; //biometalX
  var precioFonite = 30; //biometalX
  var precioScarH = 100; //biometalX
  var precioPico = 900; //biometalX
  ////////////////////////////////////////////////////////////////
  ///////////////////////////// Tenencias ////////////////////////
  var cantidadPistola = 0;
  var cantidadCancionMaluma = 0;
  var cantidadEspada = 0;
  var cantidadSpam = 0;
  var cantidadFlame = 0;
  var cantidadClaymore = 0;
  var cantidadMaincra = 0;
  var cantidadCubito = 0;
  var cantidadMuelte = 0;
  var cantidadSamael = 0;
  var cantidadWalker = 0;
  var cantidadW4LK3R = 0;
  var cantidadFlambergue = 0;
  var cantidadFonite = 0;
  var cantidadScarH = 0;
  var cantidadPico = 0;
  /////////////////////////////////////////////////////////////////
  ///////////////////////////// Restos que dan ////////////////////////
  var restosPistola = 0;
  var restosCancionMaluma = 0;
  var restosEspada = 0;
  var restosSpam = 0;
  var restosFlame = 0;
  var restosClaymore = 0;
  var restosMaincra = 0;
  var restosCubito = 0;
  var restosMuelte = 0;
  var restosSamael = 0;
  var restosWalker = 0;
  var restosW4LK3R = 0;
  var restosFlambergue = 0;
  var restosFonite = 0;
  var restosScarH = 0;
  var restosPico = 0;
  var restosTotalesByObjects = 0;
  /////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////
  var color = ["green","blue","red"];
  var randomColor2 = color[Math.floor(Math.random() * color.length)];

    $("#pj").click(function(){
      blurRadius += 5;
      restos++;
      combo++;
      comboFontSize += 15;
      $("#combo").css({"display": "block","font-size": comboFontSize+"%"});
      $("#combo").append("<span>Combo X"+combo+"</span>");
      $(this).css({"filter": "drop-shadow( 1px 1px "+blurRadius+"px crimson)"});
    });

    $("#pj").mouseleave(function(){
      combo = 0;
      comboFontSize = 100;
      blurRadius = 10;
      $(this).css({"filter": "drop-shadow(0px 5px 10px black)"});
      $("#combo").css({"display": "none"});
    });

    $("#shop").click(function(){
      $("#menu-contenedor-objetos").show("slow");
      $("#menu-contenedor-stats").hide("slow");
      $("#menu-contenedor-upgrades").hide("slow");
    });

    $("#shop").dblclick(function(){
      $("#menu-contenedor-objetos").hide("slow");
    });

    $("#stats").click(function(){
      $("#menu-contenedor-objetos").hide("slow");
      $("#menu-contenedor-stats").show("slow");
      $("#menu-contenedor-upgrades").hide("slow");
    });

    $("#stats").dblclick(function(){
      $("#menu-contenedor-stats").hide("slow");
    });

    $("#upgrades").click(function(){
      $("#menu-contenedor-objetos").hide("slow");
      $("#menu-contenedor-stats").hide("slow");
      $("#menu-contenedor-upgrades").show("slow");
    });

    $("#upgrades").dblclick(function(){
      $("#menu-contenedor-upgrades").hide("slow");
    });

    //////////////////////////////////////////// Compras de objetos /////////////////////////////////////////////////////////////////
    $("#pistola").click(function(){
      if (biometal >= precioPistola) {cantidadPistola++; biometal -= precioPistola;precioPistola += 3; restosPistola += cantidadPistola/2; restosTotalesByObjects += restosPistola;}
    });
    $("#cancionMaluma").click(function(){
      if (biometal >= precioCancionMaluma) { cantidadCancionMaluma++; biometal -= precioCancionMaluma;precioCancionMaluma += 33; restosCancionMaluma += (cantidadCancionMaluma*10)/2; restosTotalesByObjects += restosCancionMaluma;}
    });
    $("#espada").click(function(){
      if (biometal >= precioEspada) { cantidadEspada++; biometal -= precioEspada;precioEspada += 103; restosEspada += (cantidadEspada*100)/2; restosTotalesByObjects += restosEspada;}
    });
    $("#spam").click(function(){
      if (biometal >= precioSpam) { cantidadSpam++; biometal -= precioSpam;precioSpam += 903; restosSpam += (cantidadSpam*1000)/2; restosTotalesByObjects += restosSpam;}
    });
    $("#flame").click(function(){
      if (chips >= precioFlame) { cantidadFlame++; chips -= precioFlame;precioFlame += 3; restosFlame += (cantidadFlame*10000)/2; restosTotalesByObjects += restosFlame;}
    });
    $("#claymore").click(function(){
      if (chips >= precioClaymore) { cantidadClaymore++; chips -= precioClaymore; precioClaymore += 33; restosClaymore += (cantidadClaymore*100000)/2; restosTotalesByObjects += restosClaymore;}
    });
    $("#maincra").click(function(){
      if (chips >= precioMaincra) { cantidadMaincra++; chips -= precioMaincra; precioMaincra += 103; restosMaincra += (cantidadMaincra*1000000)/2; restosTotalesByObjects += restosMaincra;}
    });
    $("#cubito").click(function(){
      if (chips >= precioCubito) { cantidadCubito++; chips -= precioCubito; precioCubito += 903; restosCubito += (cantidadCubito*10000000)/2; restosTotalesByObjects += restosCubito;}
    });
    $("#muelte").click(function(){
      if (subTanks >= precioMuelte) { cantidadMuelte++; subTanks -= precioMuelte; precioMuelte += 3; restosMuelte += (cantidadMuelte*100000000)/2; restosTotalesByObjects += restosMuelte;}
    });
    $("#samael").click(function(){
      if (subTanks >= precioSamael) { cantidadSamael++; subTanks -= precioSamael; precioSamael += 33; restosSamael += (cantidadSamael*1000000000)/2; restosTotalesByObjects += restosSamael;}
    });
    $("#walker").click(function(){
      if (subTanks >= precioWalker) { cantidadWalker++; subTanks -= precioWalker; precioWalker += 103; restosWalker += (cantidadWalker*10000000000)/2; restosTotalesByObjects += restosWalker;}
    });
    $("#w4lk3r").click(function(){
      if (subTanks >= precioW4LK3R) { cantidadW4LK3R++; subTanks -= precioW4LK3R; precioW4LK3R += 903; restosW4LK3R += (cantidadW4LK3R*100000000000)/2; restosTotalesByObjects += restosW4LK3R;}
    });
    $("#flambergue").click(function(){
      if (biometalX >= precioFlambergue) { cantidadFlambergue++; biometalX -= precioFlambergue; precioFlambergue += 3; restosFlambergue += (cantidadFlambergue*1000000000000)/2; restosTotalesByObjects += restosFlambergue;}
    });
    $("#fonite").click(function(){
      if (biometalX >= precioFonite) { cantidadFonite++; biometalX -= precioFonite; precioFonite += 33;  restosFonite += (cantidadFonite*10000000000000)/2; restosTotalesByObjects += restosFonite;}
    });
    $("#scar-h").click(function(){
      if (biometalX >= 900) { cantidadScarH++; biometalX -= precioScarH; precioScarH += 103; restosScarH += (cantidadScarH*100000000000000)/2; restosTotalesByObjects += restosScarH;}
    });
    $("#pico").click(function(){
      if (biometalX >= 900) { cantidadPico++; biometalX -= 900; precioPico += 903; restosPico += (cantidadPico*100000000000000)/2; restosTotalesByObjects += restosPico;}
    });
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  $("#fuse").click(function(){
      //if (restos >= 10) {biometal++; restos -= 10;}
      if (restos >= 10) {biometal+= Math.floor(restos/10); rest0s = restos % 10; restos = rest0s;}
      if (biometal >= 1000) {chips+=Math.floor(biometal/1000); bi0metal = biometal % 1000; biometal = bi0metal;}
      if (chips >= 1200) {subTanks+=Math.floor(chips/1200); ch1ps = chips % 1200; chips = ch1ps;}
      if (subTanks >= 950) {biometalX+=Math.floor(subTanks/950); subT4nks = subTanks % 950; subTanks = subT4nks;} /////////////////////// NO VAAAAAAAAAAAAAAAAAAAAAAAA //////////////////////
  });
  ////////////////////////////////////////////////////////////////////////////////////////// Para actualizar los parámetros todo el rato ////////////////////////////////////////////
  setInterval(displayParameters, 100);
  function displayParameters() {
    $("#restos").html("Restos = "+restos);
    $("#biometal").html("Biometal = "+biometal);
    $("#chips").html("Chips = "+chips);
    $("#subTanks").html("SubTanks = "+subTanks);
    $("#biometalX").html("Biometal-X = "+biometalX);
    ///////////////////// Precio //////////////////////////
    $("#precioPistola").html("Cuesta: "+precioPistola+" Biometales");
    $("#precioCancionMaluma").html("Cuesta: "+precioCancionMaluma+" Biometales");
    $("#precioEspada").html("Cuesta: "+precioEspada+" Biometales");
    $("#precioSpam").html("Cuesta: "+precioSpam+" Biometales");
    $("#precioFlame").html("Cuesta: "+precioFlame+" Chips");
    $("#precioClaymore").html("Cuesta: "+precioClaymore+" Chips");
    $("#precioMaincra").html("Cuesta: "+precioMaincra+" Chips");
    $("#precioCubito").html("Cuesta: "+precioCubito+" Chips");
    $("#precioMuelte").html("Cuesta: "+precioMuelte+" SubTanks");
    $("#precioSamael").html("Cuesta: "+precioSamael+" SubTanks");
    $("#precioWalker").html("Cuesta: "+precioWalker+" SubTanks");
    $("#precioW4LK3R").html("Cuesta: "+precioW4LK3R+" SubTanks");
    $("#precioFlambergue").html("Cuesta: "+precioFlambergue+" Biometal-X");
    $("#precioFonite").html("Cuesta: "+precioFonite+" Biometal-X");
    $("#precioScarH").html("Cuesta: "+precioScarH+" Biometal-X");
    $("#precioPico").html("Cuesta: "+precioPico+" Biometal-X");
    ///////////////////////////////////////
    /////// Tenencias ///////////
    $("#tenenciasPistola").html("Tienes: "+cantidadPistola);
    $("#tenenciasCancionMaluma").html("Tienes: "+cantidadCancionMaluma);
    $("#tenenciasEspada").html("Tienes: "+cantidadEspada);
    $("#tenenciasSpam").html("Tienes: "+cantidadSpam);
    $("#tenenciasFlame").html("Tienes: "+cantidadFlame);
    $("#tenenciasClaymore").html("Tienes: "+cantidadClaymore);
    $("#tenenciasMaincra").html("Tienes: "+cantidadMaincra);
    $("#tenenciasCubito").html("Tienes: "+cantidadCubito);
    $("#tenenciasMuelte").html("Tienes: "+cantidadMuelte);
    $("#tenenciasSamael").html("Tienes: "+cantidadSamael);
    $("#tenenciasWalker").html("Tienes: "+cantidadWalker);
    $("#tenenciasW4LK3R").html("Tienes: "+cantidadW4LK3R);
    $("#tenenciasFlambergue").html("Tienes: "+cantidadFlambergue);
    $("#tenenciasFonite").html("Tienes: "+cantidadFonite);
    $("#tenenciasScarH").html("Tienes: "+cantidadScarH);
    $("#tenenciasPico").html("Tienes: "+cantidadPico);
    /////////////////////////////////////////////////////////////////
    ///////////////////////////// Restos que dan ////////////////////////
    $("#restosTotalesByObjects").html("Restos Totales: "+restosTotalesByObjects+"/s");
    $("#restosPistola").html("Pistola: "+restosPistola+"/s");
    $("#restosCancionMaluma").html("Cancion Maluma: "+restosCancionMaluma+"/s");
    $("#restosEspada").html("Espada: "+restosEspada+"/s");
    $("#restosSpam").html("Spam: "+restosSpam+"/s");
    $("#restosFlame").html("Flame: "+restosFlame+"/s");
    $("#restosClaymore").html("Claymore: "+restosClaymore+"/s");
    $("#restosMaincra").html("Maincra: "+restosMaincra+"/s");
    $("#restosCubito").html("Cubito: "+restosCubito+"/s");
    $("#restosMuelte").html("Muelte: "+restosMuelte+"/s");
    $("#restosSamael").html("Samael: "+restosSamael+"/s");
    $("#restosWalker").html("Walker: "+restosWalker+"/s");
    $("#restosW4LK3R").html("W4LK3R: "+restosW4LK3R+"/s");
    $("#restosFlambergue").html("Flambergue: "+restosFlambergue+"/s");
    $("#restosFonite").html("Fonite: "+restosFonite+"/s");
    $("#restosScarH").html("Scar-H: "+restosScarH+"/s");
    $("#restosPico").html("Pico: "+restosPico+"/s");
    /////////////////////////////////////////////////////////////////
  }
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  setInterval(moneyTies, 1000); /////////////////////////////////////// Acaba esto puto desgraciado ///////////////////////////////////////////////////
  function moneyTies() {
    restos += cantidadPistola            /2;
    restos += (cantidadCancionMaluma  *10)/2;
    restos += (cantidadEspada         *100)/2;
    restos += (cantidadSpam           *1000)/2;
    restos += (cantidadFlame          *10000)/2;
    restos += (cantidadClaymore       *100000)/2;
    restos += (cantidadMaincra        *1000000)/2;
    restos += (cantidadCubito         *10000000)/2;
    restos += (cantidadMuelte         *100000000)/2;
    restos += (cantidadSamael         *1000000000)/2;
    restos += (cantidadWalker         *10000000000)/2;
    restos += (cantidadW4LK3R         *100000000000)/2;
    restos += (cantidadFlambergue     *1000000000000)/2;
    restos += (cantidadFonite         *10000000000000)/2;
    restos += (cantidadScarH          *100000000000000)/2;
    restos += (cantidadPico           *10000000000000000)/2;
  }

});
