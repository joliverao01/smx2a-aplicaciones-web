$(document).on('keypress', function(e) {
  console.log(e.which);
  var adventurerWalk = document.createElement("audio");
    adventurerWalk.src = "sound/adventurer-walk.mp3";

  var adventurerJump = document.createElement("audio");
    adventurerJump.src = "sound/adventurer-jump.mp3";

  var adventurerBow = document.createElement("audio");
    adventurerBow.src = "sound/adventurer-bow.mp3";

  var adventurerSword = document.createElement("audio");
    adventurerSword.src = "sound/adventurer-sword.mp3";


   var movementXA = 3;

   var elementS = $("#skeleton");
   var positionXS = elementS.position();
   var positionYS = elementS.position();

   var elementA = $("#adventurer");
   var positionXA = elementA.position();
   var positionYA = elementA.position();

   if (e.which === 119) {
       $('#keyW').css({'background': 'rgba(255, 255, 0 ,0.8)'});
       adventurerJump.play();
       adventurerWalk.pause();
       adventurerBow.pause();
       adventurerSword.pause();
       $('#adventurer').css({'background': 'url("img/adventurer/adventurer-jump.png")', 'background-size': '500px 50px', 'animation': 'adventurer-jump 1.7s steps(7) 1 alternate, jump 1s steps(1000) 1 forwards', 'width': '50px'});
   };
   // Key D
   if (e.which === 100) {
       $('#keyD').css({'background': 'rgba(255, 255, 0 ,0.8)'});
       adventurerJump.pause();
       adventurerSword.pause();
       adventurerWalk.play();
       adventurerBow.pause();
       $('#adventurer').css({'background': 'url("img/adventurer/adventurer-walk.png")', 'transform': 'rotateY(360deg)', 'background-size': '400px 50px', 'animation': 'adventurer-walk 1s steps(6) infinite forwards', 'width': '50px'});
       var newPositionLeft =  $("#adventurer").position().left + movementXA;
       $("#adventurer").css({left: newPositionLeft});
   };
   if (e.which === 115) {
     adventurerJump.pause();
     adventurerWalk.pause();
     adventurerSword.pause();
     adventurerBow.pause();
       $('#keyS').css({'background': 'rgba(255, 255, 0 ,0.8)'});
       $('#adventurer').css({'background': 'url("img/adventurer/adventurer-squat.png")', 'background-size': '300px 50px', 'animation': 'squat 1s steps(4) infinite forwards', 'width': '50px'});
   };
   if (e.which === 97) {
      adventurerJump.pause();
      adventurerWalk.play();
      adventurerBow.pause();
      adventurerSword.pause();
       $('#keyA').css({'background': 'rgba(255, 255, 0 ,0.8)'});
       $('#adventurer').css({'background': 'url("img/adventurer/adventurer-walk.png")', 'transform': 'rotateY(180deg)', 'background-size': '400px 50px', 'animation': 'adventurer-walk 1s steps(6) infinite forwards', 'width': '50px'});
       var newPositionLeft =  $("#adventurer").position().left - movementXA;
       $("#adventurer").css({left: newPositionLeft});
   };
   if (e.which === 32) {
       adventurerJump.pause();
       adventurerWalk.pause();
       adventurerBow.play();
       adventurerSword.pause();
       $('#keySPACE').css({'background': 'rgba(255, 255, 0 ,0.8)'});
       $('#adventurer').css({'background': 'url("img/adventurer/adventurer-bow.png")', 'background-size': '400px 50px', 'animation': 'adventurer-bow 3s steps(6) 1 forwards', 'width': '50px'});
   };
   if (e.which === 101) {
       adventurerJump.pause();
       adventurerWalk.pause();
       adventurerBow.pause();
       adventurerSword.play();
       $('#keyE').css({'background': 'rgba(255, 255, 0 ,0.8)'});
       $('#adventurer').css({'background': 'url("img/adventurer/adventurer-attack.png")', 'background-size': '400px 50px', 'animation': 'attack 1s steps(6) 1 forwards', 'width': '70px'});
   };

   // Key Q
   if (e.which === 113) {
     var elementS = $("#skeleton");
     var positionXS = elementS.position();
     var positionYS = elementS.position();
     console.log('/////////////////////////////////// POSITION LEFT SKELETON ////////////////////////////////');
     console.log('Left:' + positionXS.left);
     console.log('/////////////////////////////////// POSITION TOP SKELETON /////////////////////////////////');
     console.log('Top:' + positionYS.top);
     console.log('///////////////////////////////////////////////////////////////////////////////////////////');

     console.log();

     var elementA = $("#adventurer");
     var positionXA = elementA.position();
     var positionYA = elementA.position();
     console.log('/////////////////////////////////// POSITION LEFT ADVENTURER ////////////////////////////////');
     console.log('Left:' + positionXA.left);
     console.log('/////////////////////////////////// POSITION TOP SKELETON ///////////////////////////////////');
     console.log('Top:' + positionYA.top);
     console.log('/////////////////////////////////////////////////////////////////////////////////////////////');
   };

  var timer = function() {
    $('.key').css({'background': 'rgba(22, 22, 22 ,0.8)'});
  };
  setTimeout(timer, 100);

  var arrowMovement;
  var arrowMovementLeft = 100;
  var isDone = false;
  var newPositionArrow;

  var approaching =  Math.abs($("#adventurer").position().left - $("#skeleton").position().left) <= 600;
  if (approaching && !isDone) {
      isDone = true;
    if (e.which === 32) {
        $('#keySPACE').css({'background': 'rgba(255, 255, 0 ,0.8)'});
        $('#adventurer').css({'background': 'url("img/adventurer/adventurer-bow.png")', 'background-size': '400px 50px', 'animation': 'adventurer-bow 1s steps(6) 1 forwards', 'width': '50px'});
        newPositionArrow =  $("#adventurer").position().left; //, left: newPositionArrow
        //$("#arrow").position().left = newPositionArrow;

        //$("#arrow").css({left : newPositionArrow });
        //console.log("NEW POSITION: "+newPositionArrow);
        // arrowMovement = setInterval(function() {
        // }, 200);
        $('#arrow').css({'left' : newPositionArrow, 'animation': 'fade-in 0.5s steps(1000) 1 forwards, arrow-movement 7s steps(1000) 1 forwards'});
    };
  };
});
