$(document).ready(function(){
  ////////////////////////////////////////////////////////////////////////////////////////////// AUDIO //////////////////////////////////////////////////////////////////////////////////////////////////////
  var intro = document.createElement("audio");
    intro.src = "sound/doom-ost.mp3";

  var forest = document.createElement("audio");
    forest.src = "sound/forest.mp3";

  var hills = document.createElement("audio");
    hills.src = "sound/hills.mp3";

  var industry = document.createElement("audio");
    industry.src = "sound/industry.mp3";

  var mappleWoods = document.createElement("audio");
    mappleWoods.src = "sound/mapple-woods.mp3";

  var mountains = document.createElement("audio");
    mountains.src = "sound/mountain.mp3";

  intro.play();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  $("#free-run").mouseenter(function(){
    $(".change-free-run").css({'display': 'inline'});
    $(".change-test").css({'display': 'none'});
  });

  $(".identificator").mouseleave(function(){
    $(".change-test").css({'display': 'none'});
    $(".change-free-run").css({'display': 'none'});
  });

  $(".change-test").click(function(){
    mappleWoods.pause();
    forest.pause();
    hills.pause();
    industry.pause();
    mountains.pause();
  });

  $(".change-free-run").click(function(){
    intro.pause();
    $("#skeleton").css({'display': 'none'});
    $("#keypressed").css({'display': 'none'});
    $('body').css({'background': 'black'});
    $('#test').css({'display': 'block'});
    $("#alerta-elegir").css({'display': 'none'});
    $("#alerta-controles").css({'display': 'none'});
    $("#adventurer").css({'background': 'url("img/adventurer/adventurer-run2.png")', 'background-size': '150px 50px', 'width': '25px', 'height': '50px', 'animation': 'adventurer-run 0.5s steps(6) infinite forwards, run 4s infinite alternate, adventurer-jump paused, jump paused, adventurer-walk paused, adventurer-bow paused, attack paused'});
  });

//////////////////////////////////////////////////////////////////////////////////////////////FREE RUN//////////////////////////////////////////////////////////////////////////////////////////////////////
  $("#forest").click(function(){
    mappleWoods.pause();
    forest.play();
    hills.pause();
    industry.pause();
    mountains.pause();
    $("#fondo1").css({'background': 'url("img/background/forest/1.png")', 'display': 'block', 'animation': 'parallax 1s steps(1000) infinite forwards'});
    $("#fondo2").css({'background': 'url("img/background/forest/2.png")', 'display': 'block', 'animation': 'parallax 2s steps(1000) infinite forwards'});
    $("#fondo3").css({'background': 'url("img/background/forest/3.png")', 'display': 'block', 'animation': 'parallax 3s steps(1000) infinite forwards'});
    $("#fondo4").css({'background': 'url("img/background/forest/4.png")', 'display': 'block', 'animation': 'parallax 4s steps(1000) infinite forwards'});
    $("#fondo5").css({'background': 'url("img/background/forest/5.png")', 'display': 'block', 'animation': 'parallax 6s steps(1000) infinite forwards'});
    $("#fondo6").css({'background': 'url("img/background/forest/6.png")', 'display': 'block', 'animation': 'parallax 5s steps(1000) infinite forwards'});
    $("#fondo7").css({'background': 'url("img/background/forest/7.png")', 'display': 'block', 'animation': 'parallax 7s steps(1000) infinite forwards'});
    $("#fondo8").css({'background': 'url("img/background/forest/8.png")', 'display': 'block', 'animation': 'parallax 9s steps(1000) infinite forwards'});
    $("#fondo9").css({'background': 'url("img/background/forest/9.png")', 'display': 'block', 'animation': 'parallax 8s steps(1000) infinite forwards'});
    $("#fondo10").css({'background': 'url("img/background/forest/10.png")', 'display': 'block', 'animation': 'parallax 10s steps(1000) infinite forwards'});
    $("#fondo11").css({'background': 'url("img/background/forest/11.png")', 'display': 'block', 'animation': 'parallax 11s steps(1000) infinite forwards'});
    $("#adventurer").css({'top': '683px', 'display': 'block'});
    $(".fondo").css({'background-size': '928px 793px'});
  });

  $("#hills").click(function(){
    mappleWoods.pause();
    forest.pause();
    hills.play();
    industry.pause();
    mountains.pause();
    $("#fondo1").css({'background': 'url("img/background/hill/1.png")', 'display': 'block', 'animation': 'parallax 1s steps(1000) infinite forwards'});
    $("#fondo2").css({'background': 'url("img/background/hill/2.png")', 'display': 'block', 'animation': 'parallax 2s steps(1000) infinite forwards'});
    $("#fondo3").css({'background': 'url("img/background/hill/3.png")', 'display': 'block', 'animation': 'parallax 3s steps(1000) infinite forwards'});
    $("#fondo4").css({'background': 'url("img/background/hill/4.png")', 'display': 'block', 'animation': 'parallax 4s steps(1000) infinite forwards'});
    $("#fondo5").css({'background': 'url("img/background/hill/5.png")', 'display': 'block', 'animation': 'parallax 5s steps(1000) infinite forwards'});
    $("#fondo6").css({'background': 'url("img/background/hill/6.png")', 'display': 'block', 'animation': 'parallax 6s steps(1000) infinite forwards'});
    $("#fondo7").css({'display': 'none'});
    $("#fondo8").css({'display': 'none'});
    $("#fondo9").css({'display': 'none'});
    $("#fondo10").css({'display': 'none")'});
    $("#fondo11").css({'display': 'none")'});
    $("#adventurer").css({'top': '700px', 'display': 'block'});
    $(".fondo").css({'background-size': '928px 793px'});
  });

  $("#industry").click(function(){
    mappleWoods.pause();
    forest.pause();
    hills.pause();
    industry.play();
    mountains.pause();
    $("#fondo1").css({'background': 'url("img/background/hill/2.png")', 'display': 'block', 'animation': 'parallax 1s steps(1000) infinite forwards'});
    $("#fondo2").css({'background': 'url("img/background/industry/1.png")', 'display': 'block', 'animation': 'parallax 2s steps(1000) infinite forwards'});
    $("#fondo3").css({'background': 'url("img/background/industry/3.png")', 'display': 'block', 'animation': 'parallax 3s steps(1000) infinite forwards'});
    $("#fondo4").css({'background': 'url("img/background/industry/4.png")', 'display': 'block', 'animation': 'parallax 4s steps(1000) infinite forwards'});
    $("#fondo5").css({'background': 'url("img/background/industry/5.png")', 'display': 'block', 'animation': 'parallax 5s steps(1000) infinite forwards'});
    $("#fondo6").css({'display': 'none'});
    $("#fondo7").css({'display': 'none'});
    $("#fondo8").css({'display': 'none'});
    $("#fondo9").css({'display': 'none'});
    $("#fondo10").css({'display': 'none")'});
    $("#fondo11").css({'display': 'none")'});
    $("#adventurer").css({'top': '700px', 'display': 'block'});
    $(".fondo").css({'background-size': '928px 793px'});
  });

  $("#mountains").click(function(){
    mappleWoods.pause();
    forest.pause();
    hills.pause();
    industry.pause();
    mountains.play();
    $("#fondo1").css({'background': 'url("img/background/mountains/1.png")', 'display': 'block', 'animation': 'parallax 1s steps(1000) infinite forwards'});
    $("#fondo2").css({'background': 'url("img/background/hill/2.png")', 'display': 'block', 'animation': 'parallax 2s steps(1000) infinite forwards'});
    $("#fondo3").css({'background': 'url("img/background/mountains/2.png")', 'display': 'block', 'animation': 'parallax 3s steps(1000) infinite forwards'});
    $("#fondo4").css({'background': 'url("img/background/mountains/3.png")', 'display': 'block', 'animation': 'parallax 4s steps(1000) infinite forwards'});
    $("#fondo5").css({'background': 'url("img/background/mountains/4.png")', 'display': 'block', 'animation': 'parallax 5s steps(1000) infinite forwards'});
    $("#fondo6").css({'background': 'url("img/background/mountains/5.png")', 'display': 'block', 'animation': 'parallax 6s steps(1000) infinite forwards'});
    $("#fondo7").css({'background': 'url("img/background/mountains/6.png")', 'display': 'block', 'animation': 'parallax 7s steps(1000) infinite forwards'});
    $("#fondo8").css({'background': 'url("img/background/mountains/7.png")', 'display': 'block', 'animation': 'parallax 8s steps(1000) infinite forwards'});
    $("#fondo9").css({'background': 'url("img/background/mountains/8.png")', 'display': 'block', 'animation': 'parallax 9s steps(1000) infinite forwards'});
    $("#fondo10").css({'background': 'url("img/background/mountains/9.png")', 'display': 'block', 'animation': 'parallax 10s steps(1000) infinite forwards'});
    $("#fondo11").css({'background': 'url("img/background/mountains/10.png")', 'display': 'block', 'animation': 'parallax 11s steps(1000) infinite forwards'});
    $("#adventurer").css({'top': '700px', 'display': 'block'});
    $(".fondo").css({'background-size': '928px 793px'});
  });

  $("#maple-woods").click(function(){
      mappleWoods.play();
      forest.pause();
      hills.pause();
      industry.pause();
      mountains.pause();
    $("#fondo1").css({'background': 'url("img/background/hill/2.png")', 'display': 'block', 'animation': 'parallax 1s steps(1000) infinite forwards'});
    $("#fondo2").css({'background': 'url("img/background/maple-woods/1.png")', 'display': 'block', 'animation': 'parallax 2s steps(1000) infinite forwards'});
    $("#fondo3").css({'background': 'url("img/background/maple-woods/2.png")', 'display': 'block', 'animation': 'parallax 3s steps(1000) infinite forwards'});
    $("#fondo4").css({'background': 'url("img/background/maple-woods/3.png")', 'display': 'block', 'animation': 'parallax 4s steps(1000) infinite forwards'});
    $("#fondo5").css({'background': 'url("img/background/maple-woods/4.png")', 'display': 'block', 'animation': 'parallax 5s steps(1000) infinite forwards'});
    $("#fondo6").css({'display': 'none'});
    $("#fondo7").css({'display': 'none'});
    $("#fondo8").css({'display': 'none'});
    $("#fondo9").css({'display': 'none'});
    $("#fondo10").css({'display': 'none")'});
    $("#fondo11").css({'display': 'none")'});
    $("#adventurer").css({'top': '700px', 'display': 'block'});
    $(".fondo").css({'background-size': '928px 793px'});
  });

});
