$(document).ready(function() {

  var forest = document.createElement("audio");
    forest.src = "sound/forest.mp3";
    forest.volume = 0.5;

  var hills = document.createElement("audio");
    hills.src = "sound/hills.mp3";
    hills.volume = 0.5;

  var industry = document.createElement("audio");
    industry.src = "sound/industry.mp3";
    industry.volume = 0.5;

  var mappleWoods = document.createElement("audio");
    mappleWoods.src = "sound/mapple-woods.mp3";
    mappleWoods.volume = 0.5;
  var mountains = document.createElement("audio");
    mountains.src = "sound/mountain.mp3";
    mountains.volume = 0.5;

    var fight = document.createElement("audio");
      fight.src = "sound/fight.mp3";

  var DIO_JOTARO = document.createElement("audio");
    DIO_JOTARO.src = "sound/DIO-JOTARO.mp3";

  var stageClear = document.createElement("audio");
    stageClear.src = "sound/stage-clear.mp3";

  var heehee = document.createElement("audio");
    heehee.src = "sound/heehee.mp3";

  var dota = document.createElement("audio");
    dota.src = "sound/dota.mp3";

  $("#test").mouseenter(function(){
    $(".change-test").css({'display': 'inline'});
    $(".change-free-run").css({'display': 'none'});
  });


  $(".change-free-run").click(function(){
    mappleWoods.pause();
    forest.pause();
    hills.pause();
    industry.pause();
    mountains.pause();
  });

  $(".change-test").click(function(){
    $("#keypressed").css({'display': 'block'});
    $('body').css({'background': 'black'});
    $("#alerta-elegir").css({'display': 'none'});
    $("#alerta-controles").css({'display': 'block'});
    $('.fondo').css({'animation': 'parallax paused'});
    $('#adventurer').css({'background': 'url("img/adventurer/adventurer-idle.png")', 'background-size': '250px 50px', 'animation': 'adventurer-idle 1s steps(4) infinite forwards', 'width': '50px'});
    $('#skeleton').css({'display': 'block', 'background': 'url("img/skeleton/skeleton-walk.png")', 'background-size': '600px 70px', 'animation': 'skeleton-walk 1s steps(13) infinite alternate, skeleton-walk-movement-go 8s steps(1000) 1 forwards, skeleton-walk-movement-come 40s steps(1000) 1 8s forwards', 'width': '46px', 'height': '70px'});
  ///////////////////////////////////////////////////// APPROACHING //////////////////////////////////////////////////////////////////////////
      console.log($("#adventurer").position().left);
      console.log($("#skeleton").position().left);

      var timerApproaching;
      var die = false;
      var live = false;

      var isDone = false;
      var cinematic = false;
      var movementXS = 3;
      var skeletonNewPositionLeft =  $("#skeleton").position().left + movementXS;
      function myStartFunction() {
          timerApproaching = setInterval(function() {
            var oldPositionSkeleton =  $("#skeleton").position().left;
            var approaching =  Math.abs($("#adventurer").position().left - $("#skeleton").position().left) <= 200;
            if (approaching && !isDone) {
              isDone = true;
              $('#skeleton').css({'background': 'url("img/skeleton/skeleton-react.png")', 'animation': 'skeleton-walk paused, skeleton-react 0.7s steps(4) 1 backwards', 'background-size': '180px 70px', 'width': '45px', 'height': '70px', left: oldPositionSkeleton});
              $('.fondo, .personajes, #keypressed, .alerta, .identificator').css({'animation': 'approaching 2s steps(1000) 1 backwards'});
              var timerCinematic = function(){
                if (cinematic = true) {
                  cinematic = true;
                  $('.fondo, .personajes, #keypressed, .alerta, .identificator').css({'display': 'none'});
                    DIO_JOTARO.play();
                  $('body').css({'background': 'url("img/background/DIO-JOTARO.gif")', 'background-size': '100%'});
                  forest.volume = 0.1;
                  hills.volume = 0.1;
                  industry.volume = 0.1;
                  mappleWoods.volume = 0.1;
                  mountains.volume = 0.1;
                };
              }; setTimeout(timerCinematic, 1050);

              var timerTurnBlack = function(){
                $('body').css({'background': 'black', 'background-size': '100%'});
                $('.fondo, .personajes').css({'display': 'block'});
                $('#alerta-RUN').css({'display': 'block'});
                mappleWoods.pause();
                forest.pause();
                hills.pause();
                industry.pause();
                mountains.pause();

                //Intento de evento
                var timerRUN = function(){
                  fight.play();
                  $('#skeleton').css({'background': 'url("img/skeleton/skeleton-attack.png")', 'animation': 'skeleton-react paused, skeleton-attack 2s steps(18) infinite forwards, skeleton-walk-movement-come 40s steps(5000) 1 forwards', 'background-size': '1200px 70px', 'width': '60px', 'height': '70px'});
                  timerApproaching = setInterval(function() {

                  oldPositionSkeleton =  $("#skeleton").position().left;

                  var skeletonKills = (Math.abs($("#adventurer").position().left - $("#skeleton").position().left) <= 50);
                    if (skeletonKills && !die) {
                      die = true;
                      fight.pause();
                      stageClear.play();
                      $('#skeleton').css({'background': 'url("img/skeleton/skeleton-idle.png")', 'animation': 'skeleton-attack paused, skeleton-walk-movement-come paused, skeleton-idle 1s steps(11) infinite', 'background-size': '520px 70px', 'width': '46px', 'height': '70px', left: oldPositionSkeleton}); //Aqui va donde el gana y hace el idle.
                      $('#adventurer').css({'background': 'url("img/adventurer/adventurer-die.png")', 'background-size': '500px 50px', 'height':'50px', 'width': '50px', 'animation': 'adventurer-walk paused, adventurer-jump paused, adventurer-bow paused, squat paused, die 1s steps(7) forwards, fade-out 1.5s steps(7) 1 forwards'});
                      var sonidoAyuwoki = function(){
                        heehee.play();
                        $('.fondo, .personajes, #keypressed, .alerta, .identificator').css({'animation': 'fade-out 1s steps(1000) 1 forwards'});
                      }; setTimeout(sonidoAyuwoki, 5000);
                      var ayuwokiAppears = function(){
                        $('body').css({'background': 'url("img/background/ayuwoki.gif")', 'background-size':'100%', 'animation': 'fade-in 5s steps(1000) 1 forwards'});
                        $('#badEnding').css({'display': 'block', 'animation': 'fade-in 7s steps(1000) 1 forwards'});
                      }; setTimeout(ayuwokiAppears, 6000);
                    };

                 var adventurerKills = (Math.abs($("#arrow").position().left - $("#skeleton").position().left) <= 50);
                    if (adventurerKills && !live) {
                      live = true;
                      fight.pause();
                      stageClear.play();
                    $('#skeleton').css({'background': 'url("img/skeleton/skeleton-die.png")', 'animation': 'skeleton-react paused, skeleton-attack paused, skeleton-walk-movement-come paused, skeleton-die 2s steps(15) 1 forwards, fade-out 2s steps(1000) 1 forwards', 'background-size': '1000px 70px', 'width': '60px', 'height': '70px'}); //Aqui va donde lo matan
                    $('#adventurer').css({'background': 'url("img/adventurer/adventurer-idle.png")', 'background-size': '250px 50px', 'animation': 'adventurer-idle 1s steps(4) infinite forwards', 'width': '50px'}); //Aqui va donde gana la pelea y pone el idle
                    $('#arrow').css({'animation': 'fade-out 0.5s steps(1000) 1 forwards'});

                    var sonidoRicardo = function(){
                      dota.play();
                      $('.fondo, .personajes, #keypressed, .alerta, .identificator').css({'animation': 'fade-out 1s steps(1000) 1 forwards'});
                    }; setTimeout(sonidoRicardo, 5000);
                    var ricardoAppears = function(){
                      $('body').css({'background': 'url("img/background/ricardoShooting.gif")', 'background-size':'100%', 'animation': 'fade-in 5s steps(1000) 1 forwards'});
                      $('#goodEnding').css({'display': 'block', 'animation': 'fade-in 7s steps(1000) 1 forwards'});
                    }; setTimeout(ricardoAppears, 6000);
                    };

                }, 150);
                }; setTimeout(timerRUN, 3000);

              }; setTimeout(timerTurnBlack, 16000);

              // console.log(approaching);
              // console.log($("#adventurer").position().left);
              // console.log($("#skeleton").position().left);
            };
          }, 200);
       };

       myStartFunction();

      var elementS = $("#skeleton");
      var positionXS = elementS.position();
      var positionYS = elementS.position();

      var elementA = $("#adventurer");
      var positionXA = elementA.position();
      var positionYA = elementA.position();

      /////////////////////////////////////////// El TIMER DEL ESQUELETO PARA QUE DE LA VUELTA //////////////////////////////////////////////
      var timerCome;
      var come = false;
      timerCome = setInterval(function() {
        var goingToCome = $("#skeleton").position().left >= 1725;
        if (goingToCome && !come) {
          var come = true;
          $('#skeleton').css({'transform': 'scaleX(-1)'});
        }
      }, 200);
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $("#alerta-controles").mouseenter(function(){
      $("#alerta-controles").css({'filter': 'opacity(0.3)', 'filter': 'blur(5px)'});
    });

    $("#alerta-controles").mouseleave(function(){
      $("#alerta-controles").css({'filter': 'none'});
    });

  });


  $("#forest-test").click(function(){
    mappleWoods.pause();
    forest.play();
    hills.pause();
    industry.pause();
    mountains.pause();
    $("#fondo1").css({'background': 'url("img/background/forest/1.png")', 'display': 'block'});
    $("#fondo2").css({'background': 'url("img/background/forest/2.png")', 'display': 'block'});
    $("#fondo3").css({'background': 'url("img/background/forest/3.png")', 'display': 'block'});
    $("#fondo4").css({'background': 'url("img/background/forest/4.png")', 'display': 'block'});
    $("#fondo5").css({'background': 'url("img/background/forest/5.png")', 'display': 'block'});
    $("#fondo6").css({'background': 'url("img/background/forest/6.png")', 'display': 'block'});
    $("#fondo7").css({'background': 'url("img/background/forest/7.png")', 'display': 'block'});
    $("#fondo8").css({'background': 'url("img/background/forest/8.png")', 'display': 'block'});
    $("#fondo9").css({'background': 'url("img/background/forest/9.png")', 'display': 'block',  'animation': 'parallax paused'});
    $("#fondo10").css({'background': 'url("img/background/forest/10.png")', 'display': 'block',  'animation': 'parallax paused'});
    $("#fondo11").css({'background': 'url("img/background/forest/11.png")', 'display': 'block'});
    $("#adventurer").css({'top': '683px', 'display': 'block', 'animation': 'adventurer-run paused, run paused, adventurer-idle 1s steps(4) infinite forwards'});
    $('#arrow').css({'top': '700px', 'opacity': '0'});
    $("#skeleton").css({'top': '660px', 'display': 'block'});
    $(".fondo").css({'background-size': '928px 793px'});
  });

  $("#hills-test").click(function(){
    mappleWoods.pause();
    forest.pause();
    hills.play();
    industry.pause();
    mountains.pause();
    $("#fondo1").css({'background': 'url("img/background/hill/1.png")', 'display': 'block'});
    $("#fondo2").css({'background': 'url("img/background/hill/2.png")', 'display': 'block'});
    $("#fondo3").css({'background': 'url("img/background/hill/3.png")', 'display': 'block'});
    $("#fondo4").css({'background': 'url("img/background/hill/4.png")', 'display': 'block'});
    $("#fondo5").css({'background': 'url("img/background/hill/5.png")', 'display': 'block'});
    $("#fondo6").css({'background': 'url("img/background/hill/6.png")', 'display': 'block'});
    $("#fondo7").css({'display': 'none'});
    $("#fondo8").css({'display': 'none'});
    $("#fondo9").css({'display': 'none'});
    $("#fondo10").css({'display': 'none")'});
    $("#fondo11").css({'display': 'none")'});
    $("#adventurer").css({'top': '700px', 'display': 'block', 'animation': 'adventurer-run paused, run paused, adventurer-idle 1s steps(4) infinite forwards'});
    $('#arrow').css({'top': '719px', 'opacity': '0'});
    $("#skeleton").css({'top': '673px', 'display': 'block'});
    $(".fondo").css({'background-size': '928px 793px'});
  });

  $("#industry-test").click(function(){
    mappleWoods.pause();
    forest.pause();
    hills.pause();
    industry.play();
    mountains.pause();
    $("#fondo1").css({'background': 'url("img/background/hill/2.png")', 'display': 'block'});
    $("#fondo2").css({'background': 'url("img/background/industry/1.png")', 'display': 'block'});
    $("#fondo3").css({'background': 'url("img/background/industry/3.png")', 'display': 'block'});
    $("#fondo4").css({'background': 'url("img/background/industry/4.png")', 'display': 'block'});
    $("#fondo5").css({'background': 'url("img/background/industry/5.png")', 'display': 'block'});
    $("#fondo6").css({'display': 'none'});
    $("#fondo7").css({'display': 'none'});
    $("#fondo8").css({'display': 'none'});
    $("#fondo9").css({'display': 'none'});
    $("#fondo10").css({'display': 'none")'});
    $("#fondo11").css({'display': 'none")'});
    $("#adventurer").css({'top': '700px', 'display': 'block', 'animation': 'adventurer-run paused, run paused, adventurer-idle 1s steps(4) infinite forwards'});
    $('#arrow').css({'top': '719px', 'opacity': '0'});
    $("#skeleton").css({'top': '673px', 'display': 'block'});
    $(".fondo").css({'background-size': '928px 793px'});
  });

  $("#mountains-test").click(function(){
    mappleWoods.pause();
    forest.pause();
    hills.pause();
    industry.pause();
    mountains.play();
    $("#fondo1").css({'background': 'url("img/background/mountains/1.png")', 'display': 'block'});
    $("#fondo2").css({'background': 'url("img/background/hill/2.png")', 'display': 'block'});
    $("#fondo3").css({'background': 'url("img/background/mountains/2.png")', 'display': 'block'});
    $("#fondo4").css({'background': 'url("img/background/mountains/3.png")', 'display': 'block'});
    $("#fondo5").css({'background': 'url("img/background/mountains/4.png")', 'display': 'block'});
    $("#fondo6").css({'background': 'url("img/background/mountains/5.png")', 'display': 'block'});
    $("#fondo7").css({'background': 'url("img/background/mountains/6.png")', 'display': 'block'});
    $("#fondo8").css({'background': 'url("img/background/mountains/7.png")', 'display': 'block'});
    $("#fondo9").css({'background': 'url("img/background/mountains/8.png")', 'display': 'block'});
    $("#fondo10").css({'background': 'url("img/background/mountains/9.png")', 'display': 'block'});
    $("#fondo11").css({'background': 'url("img/background/mountains/10.png")', 'display': 'block'});
    $("#adventurer").css({'top': '700px', 'display': 'block', 'animation': 'adventurer-run paused, run paused, adventurer-idle 1s steps(4) infinite forwards'});
    $('#arrow').css({'top': '719px', 'opacity': '0'});
    $("#skeleton").css({'top': '673px', 'display': 'block'});
    $(".fondo").css({'background-size': '928px 793px'});
  });

  $("#maple-woods-test").click(function(){
    mappleWoods.play();
    forest.pause();
    hills.pause();
    industry.pause();
    mountains.pause();
    $("#fondo1").css({'background': 'url("img/background/hill/2.png")', 'display': 'block'});
    $("#fondo2").css({'background': 'url("img/background/maple-woods/1.png")', 'display': 'block'});
    $("#fondo3").css({'background': 'url("img/background/maple-woods/2.png")', 'display': 'block'});
    $("#fondo4").css({'background': 'url("img/background/maple-woods/3.png")', 'display': 'block'});
    $("#fondo5").css({'background': 'url("img/background/maple-woods/4.png")', 'display': 'block'});
    $("#fondo6").css({'display': 'none'});
    $("#fondo7").css({'display': 'none'});
    $("#fondo8").css({'display': 'none'});
    $("#fondo9").css({'display': 'none'});
    $("#fondo10").css({'display': 'none")'});
    $("#fondo11").css({'display': 'none")'});
    $("#adventurer").css({'top': '700px', 'display': 'block', 'animation': 'adventurer-run paused, run paused, adventurer-idle 1s steps(4) infinite forwards'});
    $('#arrow').css({'top': '719px', 'opacity': '0'});
    $("#skeleton").css({'top': '673px', 'display': 'block'});
    $(".fondo").css({'background-size': '928px 793px'});
  });
});
